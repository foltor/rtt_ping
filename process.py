import os
import glob
import csv
import time
source = open("list.txt","r")
sh_file = open("ping.sh","w+")
i = 0
fields=['ip','avg ping(ms)']
t = time.localtime()
timestamp = time.strftime('%b_%d_%Y_%H%M', t)
with open(r'ping_result_'+timestamp+'.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerow(fields)
sourceLine = source.readlines()
for x in sourceLine:
    print(x)
    command = "ping "+x.strip()+" -c 3 > ./output/ping"+str(i)+".txt &"
    sh_file.write(command)
    print(command)
    i=i+1
# ping1 = ping('google.com',size=64)
sh_file.close()
os.system("./ping.sh")
time.sleep(30)
# get all the result as output as csv
files = os.listdir("output/")

for file in files :
    fileName = "./output/"+str(file)
    output_file = open(fileName,"r")
    output_file_lines = output_file.readlines()
    lines_len = len(output_file_lines)
    if lines_len > 0:
        last_line = output_file_lines[lines_len-1]
        first_line = output_file_lines[0]
        last_line_str = last_line.split('/')
        first_line_str = first_line.split(' ')
        ip = first_line_str[1]
        if len(last_line_str) > 2 :
            avg_ping = last_line_str[4]
        else:
            avg_ping = "loss comm"
        value = [ip,avg_ping]
        with open(r'ping_result_'+timestamp+'.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerow(value)
    os.remove(fileName)
